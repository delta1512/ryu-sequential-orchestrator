import json
import socket

from ryuso import Orchestrator


class Client:
    '''
    Client: The base client for the RYU orchestrator

    This class is used to send a single synchronous request to the RYU server
    according to the RYU spec used and optionally await the result.
    '''
    def __init__(self, ryu_spec: str):
        self.orchestrator = Orchestrator(ryu_spec)

    def generate_request(self, program: str, data: dict={}):
        return {}

    def call_program(self, program: str, data: dict={}, needs_result=True):
        req = self.generate_request(program, data)
        return {}
