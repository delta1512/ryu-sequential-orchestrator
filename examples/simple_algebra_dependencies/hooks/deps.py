def check_positive(data_pool: dict):
    print('Checking if the x is positive')
    x = data_pool.get('x')

    assert x > 0, 'The integer value is negative'

    return data_pool
