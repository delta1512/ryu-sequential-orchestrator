def plus_five(data_pool: dict):
    print('Running plus_five')
    data_pool['x'] = data_pool.get('x') + 5

    return data_pool


def times_ten(data_pool: dict):
    print('Running times_ten')
    data_pool['x'] = data_pool.get('x') * 10

    return data_pool


def power_two(data_pool: dict):
    print('Running power_two')
    x = data_pool.get('x')
    data_pool['x'] = x * x

    return data_pool
