from math import sqrt

def minus_five(data_pool: dict):
    print('Running minus_five')
    data_pool['x'] = data_pool.get('x') - 5

    return data_pool


def divide_ten(data_pool: dict):
    print('Running divide_ten')
    data_pool['x'] = int(data_pool.get('x') / 10)

    return data_pool


def sqrt_x(data_pool: dict):
    print('Running sqrt')
    data_pool['x'] = int(sqrt(data_pool.get('x')))

    return data_pool
